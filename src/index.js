import React from 'react';
import { Provider } from 'react-redux';
import { Store } from './store';
import Routes from './routes';

export default function App() {
  return (
    <Provider store={Store}>
      <Routes />
    </Provider>
  );

  /*
  return (
    <Page>
      <BasicForm
        onSubmit={(data) => {
          console.log('Dados submitados', data);
        }}
        fields={[
          {
            label: 'Nome',
            name: 'nome',
            autoCapitalize: 'words',
            validators: ['required'],
          },
          {
            label: 'Email',
            name: 'email',
            autoCapitalize: 'none',
            validators: ['required', 'email'],
          },
          {
            label: 'Senha',
            secureTextEntry: true,
            name: 'password',
            validators: ['required'],
          },
        ]}
      />
    </Page>
  );
  */
}
