import { combineReducers } from 'redux';
import { createStore } from 'redux';

import AppReducer from './AppReducer';

export const Store = createStore(
  combineReducers({
    app: AppReducer,
  }),
);
