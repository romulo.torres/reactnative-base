export const TYPES = {
  LOADING_START: 'LOADING_START',
  LOADING_STOP: 'LOADING_STOP',
};

const INITIAL_STATE = {
  loading: 0,
  ready: true,
};

export default (state = INITIAL_STATE, { type, payload }) => {
  switch (type) {
    case TYPES.LOADING_START:
      return { ...state, loading: state.loading + 1 };
    case TYPES.LOADING_STOP:
      return {
        ...state,
        loading: state.loading - 1 < 0 ? 0 : state.loading - 1,
      };
    default:
      return state;
  }
};
