import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { useSelector } from 'react-redux';

import SplashScreen from './screens/Splash';
import LoadingScreen from './screens/Loading';
import ExampleForm from './screens/ExampleForm';

const Stack = createStackNavigator();

function Routes() {
  const { ready, loading } = useSelector((state) => state.app);
  const { Navigator, Screen } = Stack;
  if (!ready) {
    return <SplashScreen />;
  }
  return (
    <>
      <NavigationContainer>
        <Navigator>
          <Screen name="ExampleForm" component={ExampleForm} />
        </Navigator>
      </NavigationContainer>
      {loading > 0 && <LoadingScreen />}
    </>
  );
}

export default Routes;
