import React, { useState, useEffect } from 'react';
import { TextInput, StyleSheet, Text, View } from 'react-native';
import { Validators } from '../BasicForm/validators';

export default function Input(props) {
  const {
    name,
    label,
    defaultValue,
    register,
    submited,
    validators = [],
    onChangeText = () => {},
    ...rest
  } = props;
  const [value, setValue] = useState(defaultValue || '');
  const [error, setError] = useState('');

  useEffect(() => {
    if (submited) setError(_getErrors(value));
  }, [submited]);

  function _getErrors(text) {
    for (const v of validators) {
      let err = null;
      if (typeof v === 'string') err = Validators[v](text);
      else err = v(text);
      if (err) {
        return err;
      }
    }
  }

  function _onChangeText(text) {
    setValue(text);
    setError(_getErrors(text));
    onChangeText(text);
  }

  return (
    <View style={styles.box}>
      <Text>{label}</Text>
      <TextInput
        {...rest}
        style={styles.input}
        name={name}
        value={value}
        onChangeText={_onChangeText}
      />
      {!!error && <Text style={styles.errorMesage}>{error}</Text>}
    </View>
  );
}

const styles = StyleSheet.create({
  box: {},
  input: {
    marginBottom: 20,
    borderWidth: 1,
    padding: 10,
    paddingTop: 5,
    paddingBottom: 5,
  },
  errorMesage: {
    color: 'red',
    marginTop: -20,
    marginBottom: 20,
  },
});
