import React from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';

export default function Page(props) {
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      style={styles.container}>
      <ScrollView style={styles.scroll}>
        <StatusBar /*barStyle="dark-content"*/ />
        <View style={styles.content}>{props.children}</View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'stretch',
  },
  scroll: {
    paddingLeft: 20,
    paddingRight: 20,
  },
  content: {
    paddingTop: 20,
    paddingBottom: 20,
  },
});
