export const Validators = {
  required: (value) => {
    if (typeof value === 'string') value = value.trim();
    if (!value) return 'Este campo é requerido !';

    return null;
  },
  email: (value) => {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(String(value).toLowerCase())) return 'Email inválido';
  },
};
