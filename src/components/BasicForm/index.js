import React, { useState, useEffect } from 'react';
import { Text, Button, Alert } from 'react-native';
import { Validators } from '../BasicForm/validators';
import Input from '../Input';

export default function BasicForm(props) {
  const { fields, submitTitle, onSubmit } = props;
  const [formData, setFormData] = useState({});
  const [submited, setSubmited] = useState(false);

  useEffect(() => {
    const _formData = {};
    [...fields].forEach((field) => {
      if (field.value === undefined && field.defaultValue) {
        field.value = field.defaultValue;
      }
      _formData[field.name] = field;
    });
    setFormData(_formData);
  }, []);

  function updateFormData(name, value) {
    const _formData = { ...formData };
    _formData[name].value = value;
    setFormData(_formData);
  }

  function setObjectTree(obj, path, value) {
    if (!obj) obj = {};
    while (path.length > 1) {
      const current = path.shift();
      if (!obj[current]) obj[current] = {};
      obj = obj[current];
    }
    obj[path.shift()] = value;
  }

  function formatReturn() {
    const result = {};
    for (const field of Object.values(formData)) {
      setObjectTree(result, field.name.split('.'), field.value);
    }
    return result;
  }

  function validateForm() {
    setSubmited(true);
    for (const f of Object.values(formData)) {
      for (const v of f.validators) {
        let err = null;
        if (typeof v === 'string') err = Validators[v](f.value);
        else err = v(f.value);
        if (err) {
          console.log(f);
          return false;
        }
      }
    }
    return true;
  }

  function handleSubmit() {
    if (validateForm()) {
      onSubmit(formatReturn(formData));
    } else {
      Alert.alert(
        'Atenção',
        'Dados inválidos no formulário. Por gentileza verifique e tente novamente.',
      );
    }
  }

  return (
    <>
      {fields.map((f, idx) => (
        <Input
          key={idx}
          name={f.name}
          onChangeText={(value) => updateFormData(f.name, value)}
          defaultValue={f.defaultValue}
          label={f.label || 'INFORMAR LABEL'}
          keyboardType={f.keyboardType || 'default'}
          validators={f.validators || []}
          secureTextEntry={f.secureTextEntry || false}
          autoCapitalize={f.autoCapitalize}
          submited={submited}
        />
      ))}
      <Button title={submitTitle || 'OK'} onPress={() => handleSubmit()} />
    </>
  );
}
