import { useDispatch } from 'react-redux';
import { TYPES } from '../store/AppReducer';

const useLoading = () => {
  const dispatch = useDispatch();
  return {
    loadingStart: () => {
      dispatch({ type: TYPES.LOADING_START });
    },
    loadingStop: () => {
      dispatch({ type: TYPES.LOADING_STOP });
    },
  };
};

export default useLoading;
