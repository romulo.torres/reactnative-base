import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default function SplashScreen(props) {
  return (
    <View style={styles.container}>
      <Text style={styles.txt}>ZELO</Text>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: '#333',
  },
  txt: {
    color: 'white',
    fontSize: 30,
    alignContent: 'center',
    width: '100%',
    textAlign: 'center',
  },
});
