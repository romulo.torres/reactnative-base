import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default function LoadingScreen(props) {
  return (
    <View style={styles.container}>
      <Text style={styles.txt}>Loading...</Text>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: 'rgba(0,0,0,.3)',
  },
  txt: {
    color: 'white',
    fontSize: 30,
    alignContent: 'center',
    width: '100%',
    textAlign: 'center',
  },
});
