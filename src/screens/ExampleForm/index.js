import React from 'react';
import { BasicForm, Page } from '../../components';
import useLoading from '../../hooks/loading';

export default function ExampleForm(props) {
  const { loadingStart, loadingStop } = useLoading();
  return (
    <Page>
      <BasicForm
        onSubmit={(data) => {
          console.log('oi');
          loadingStart();
          setTimeout(() => {
            loadingStop();
          }, 3000);
          console.log('Dados submitados', data);
        }}
        fields={[
          {
            label: 'Nome',
            name: 'nome',
            autoCapitalize: 'words',
            validators: ['required'],
          },
          {
            label: 'Email',
            name: 'email',
            autoCapitalize: 'none',
            validators: ['required', 'email'],
          },
          {
            label: 'Senha',
            secureTextEntry: true,
            name: 'password',
            validators: ['required'],
          },
        ]}
      />
    </Page>
  );
}
